# Visual Framework WordPress Theme

## Documentation

* [Work in Progress, Issues, Bugs](https://git.embl.de/grp-stratcom/vf-wp/issues)
* [Theme and plugin architecture](docs/architecture.md)
* [WordPress setup](docs/wordpress.md)
* [Pantheon development](docs/development.md)

## Plugin documentation

* [EMBL Taxonomy](wp-content/plugins/embl-taxonomy/README.md)
* [VF Gutenberg](wp-content/plugins/vf-gutenberg/README.md)

Containers:

* [Beta container](wp-content/plugins/vf-beta-container/README.md)
* [Breadcrumbs container](wp-content/plugins/vf-breadcrumbs-container/README.md)
* [EMBL News container](wp-content/plugins/vf-embl-news-container/README.md)
* [Global Footer container](wp-content/plugins/vf-global-footer-container/README.md)
* [Global Header container](wp-content/plugins/vf-global-header-container/README.md)

Blocks:

* [Factoid block](wp-content/plugins/vf-factoid-block/README.md)
* [Group Header block](wp-content/plugins/vf-group-header-block/README.md)
* [Jobs block](wp-content/plugins/vf-jobs-block/README.md)
* [Latest Posts block](wp-content/plugins/vf-latest-posts-block/README.md)
* [Members block](wp-content/plugins/vf-members-block/README.md)
* [Publications block](wp-content/plugins/vf-publications-block/README.md)

## Work in Progress

[**Theme demo**](http://dev-vf-theme-prototype.pantheonsite.io/) in development can be found on pantheon for now.

## Development Requirements

* Git
* Node

```bash
npm install
```
