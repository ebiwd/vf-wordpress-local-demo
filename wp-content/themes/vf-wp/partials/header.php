<?php get_template_part('partials/head'); ?>
<?php vf_header(); ?>

<div class="vf-inlay">
  <header class="embl-group-header__header embl-group-page--vf-header--bg-color">
    <?php get_template_part('partials/vf-masthead'); ?>
    <div class="embl-group-header__navigation embl-group-header__navigation--main">
      <?php get_template_part('partials/vf-navigation'); ?>
    </div>
  </header>
</div>
