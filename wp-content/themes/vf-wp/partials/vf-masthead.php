<div class="vf-masthead">
  <div class="vf-masthead__inner">
    <div class="vf-masthead__title">
      <div class="vf-masthead__title-inner">
        <h1 class="vf-masthead__heading | text--heading--xl">
          <a class="vf-masthead__heading__link" href="<?php echo home_url(); ?>"><?php echo esc_html(get_bloginfo('name')); ?></a>
        </h1>
      </div>
    </div>
  </div>
</div>
<!--/vf-masthead-->
